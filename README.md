The included Conda [`environment.yml`](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#exporting-the-environment-yml-file) should allow this to be run using [Binder](https://mybinder.org).


The following commands should be sufficient to install dependencies needed to run the four notebooks in `accuracy_measures`:
```
conda env create -f environment.yml
conda activate networks
pip install -e plot_utils
pip install -e simulations
```